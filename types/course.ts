/* eslint-disable camelcase */
export interface DraftArticle {
	course_id: string;
	title: string;
	category: string;
	content: string;
	order_key: number;
}

export interface Article extends DraftArticle {
	id: string;
	created_at: string;
	updated_at: string;
}

export interface Course {
	id: string;
	created_at: string;
	updated_at: string;
	title: string;
	description: string;
	cover_image: string;
	creator_id: string;

	articles: Article[];
}
/* eslint-enable camelcase */
