import {createClient} from '@supabase/supabase-js';
import {Article, Course} from '~/types/course';
import {User} from '~/types/user';

export const supabase = createClient(
	import.meta.env.VITE_SUPABASE_URL as string,
	import.meta.env.VITE_SUPABASE_ANON as string,
);

export const db = {
	get courses() {
		return supabase.from<Course>('courses');
	},
	get articles() {
		return supabase.from<Article>('articles');
	},
	get profiles() {
		return supabase.from<User>('profiles');
	},
};
