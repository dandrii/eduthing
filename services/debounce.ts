export const debounce = <T extends (...args: any[]) => any>(
	func: T,
	time: number,
): ((...args: Parameters<T>) => void) => {
	let timeout: NodeJS.Timeout;

	return (...args) => {
		if (timeout) {
			clearTimeout(timeout);
		}

		timeout = setTimeout(() => {
			func(...args);
			clearTimeout(timeout);
		}, time);
	};
};
