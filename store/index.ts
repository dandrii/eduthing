import { InjectionKey } from '#imports';
import {State as CourseState} from './course';
import {State as UserState} from './user';

export const key: InjectionKey<{user: UserState; course: CourseState}> =
	Symbol('STORE_KEY');
