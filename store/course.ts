import type {MutationTree, ActionTree} from 'vuex';

import type {PostgrestResponse} from '@supabase/postgrest-js';

import {db, supabase} from '~/services/supabase';
import {Article, Course, DraftArticle} from '~/types/course';

export const state = () => ({
	courses: [] as Course[],
	isSaving: false,
	nthOfOperations: 0,
});

export type State = ReturnType<typeof state>;

export const mutations: MutationTree<State> = {
	add(state, course: Course) {
		state.courses.push(course);
	},

	set(state, courses: Course[]) {
		state.courses = courses;
	},

	update(state, course: Partial<Course>) {
		const toUpdate = state.courses.find(({id}) => id === course.id);
		if (toUpdate) {
			for (const key in course) {
				toUpdate[key as keyof Course] = course[key as keyof Course] as any;
			}
		}
	},

	updateArticle(state, article: {id: string; content: string}) {
		const toUpdate = state.courses
			.flatMap(({articles}) => articles)
			.find(({id}) => id === article.id);
		if (toUpdate) {
			toUpdate.content = article.content;
		}
	},

	saving(state, status = !state.isSaving) {
		state.nthOfOperations += status ? 1 : -1;
		state.isSaving = state.nthOfOperations !== 0;
	},
};

export const actions: ActionTree<State, State> = {
	async createCourse({commit}, {title, description}: Partial<Course>) {
		const {body, error} = await db.courses.insert({
			title,
			description,
			creator_id: supabase.auth.user()!.id,
		});

		if (error) {
			throw error;
		}

		commit('add', body![0]);

		return body![0].id;
	},

	async loadCourses({commit}, id: string) {
		const query = db.courses
			.select(
				`
				*,
				articles (*)
			`,
			)
			.order('created_at', {ascending: false})
			.order('order_key' as unknown as keyof Course, {
				foreignTable: 'articles',
				ascending: true,
			});

		if (id) {
			query.eq('id', id);
		}

		const {body, error} = await query;

		if (error) {
			throw error;
		}

		if (id && body?.[0]) {
			commit('add', body[0]);
		} else {
			commit('set', body);
		}
	},

	async modifyArticles(
		{commit},
		{
			courseId,
			articles,
		}: {courseId: string; articles: (Article | DraftArticle)[]},
	) {
		commit('saving', true);

		const toUpdate = articles.filter(
			(item) => 'id' in item && item.id,
		) as Article[];
		const toAdd = articles.filter(
			(item) => !('id' in item) || !item.id,
		) as DraftArticle[];

		const modified: Article[] = [];

		if (toAdd.length) {
			const {body: created, error: insertError} = await db.articles.insert(
				toAdd,
			);
			if (insertError) {
				throw insertError;
			}

			modified.push(...created!);
		}

		if (toUpdate.length) {
			const results: PostgrestResponse<Article>[] = await Promise.all(
				toUpdate.map((article) =>
					db.articles.update({...article, id: undefined}).eq('id', article.id),
				),
			);
			const updateError = results.find(({error}) => error);
			if (updateError) {
				throw updateError;
			}

			const updated = results
				.flatMap(({body}) => body)
				.filter(Boolean) as Article[];

			modified.push(...updated);
		}

		const deleteQuery = db.articles.delete().eq('course_id', courseId);

		const idsToKeep = modified!.map(({id}) => id);

		if (idsToKeep.length) {
			deleteQuery.not('id', 'in', `(${idsToKeep})`);
		}

		await deleteQuery;

		commit('update', {
			id: courseId,
			articles: modified.sort((a, b) => a.order_key - b.order_key),
		});
		commit('saving', false);
	},

	async modifyInfo(
		{commit},
		{
			courseId,
			...data
		}: {courseId: string} & Pick<
			Course,
			'title' | 'description' | 'cover_image'
		>,
	) {
		commit('saving', true);

		await db.courses.update(data).eq('id', courseId);

		commit('update', {
			id: courseId,
			...data,
		});
		commit('saving', false);
	},

	async modifyArticleContent(
		{commit},
		{articleId, content}: {articleId: string; content: string},
	) {
		commit('saving', true);

		await db.articles.update({content}).eq('id', articleId);

		commit('saving', false);
		commit('updateArticle', {
			id: articleId,
			content,
		});
	},
};
