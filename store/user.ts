import type {MutationTree, ActionTree} from 'vuex';

import type {Provider} from '@supabase/gotrue-js';

import {User} from '~/types/user';
import {db, supabase} from '~/services/supabase';

export const state = () => ({
	email: '',
	name: '',
	username: '',
});

export type State = ReturnType<typeof state>;

export const mutations: MutationTree<State> = {
	set(state, user: Partial<State>) {
		state.email = 'email' in user ? user.email! : state.email;
		state.name = 'name' in user ? user.name! : state.name;
		state.username = 'username' in user ? user.username! : state.username;
	},
};

export const actions: ActionTree<State, State> = {
	async login(_, data: {email: string} | {provider: Provider}) {
		const {error: authError} = await supabase.auth.signIn(data);
		if (authError) {
			throw authError;
		}
	},

	async getUser({commit}) {
		const user = supabase.auth.user();
		if (!user) {
			return;
		}

		const {data, error, status} = await db.profiles
			.select('name, username')
			.single();

		if (error && status !== 406) {
			throw error;
		}

		commit('set', {
			email: user.email,
			name: data?.name || '',
			username: data?.username || '',
		});
	},

	async updateUser({commit}, user: Omit<User, 'email'>) {
		const authUser = supabase.auth.user();
		if (!authUser) {
			return;
		}

		const {error} = await db.profiles.upsert({
			id: authUser.id,
			name: user.name,
			username: user.username,
		});
		if (error) {
			throw error;
		}

		commit('set', {
			name: user.name,
			username: user.username,
		});
	},

	async logout({commit}) {
		const {error} = await supabase.auth.signOut();
		if (error) {
			throw error;
		}

		commit('set', {
			email: '',
			name: '',
			username: '',
		});
	},
};
