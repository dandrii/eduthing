import {defineNuxtConfig} from '@nuxt/bridge';

export default defineNuxtConfig({
	// Target: https://go.nuxtjs.dev/config-target
	target: 'static',

	// Global page headers: https://go.nuxtjs.dev/config-head
	head: {
		title: 'EduThing',
		htmlAttrs: {
			lang: 'en',
		},
		meta: [
			{charset: 'utf-8'},
			{name: 'viewport', content: 'width=device-width, initial-scale=1'},
			{hid: 'description', name: 'description', content: ''},
			{name: 'format-detection', content: 'telephone=no'},
		],
		link: [{rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}],
	},

	// Global CSS: https://go.nuxtjs.dev/config-css
	css: ['highlight.js/styles/nord.css', '~/assets/css/main.scss'],

	// Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
	plugins: [],

	// Auto import components: https://go.nuxtjs.dev/config-components
	components: true,

	// Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
	buildModules: ['@nuxtjs/google-fonts', '@nuxt/postcss8'],

	// Modules: https://go.nuxtjs.dev/config-modules
	modules: [
		// https://go.nuxtjs.dev/axios
		'@nuxtjs/axios',
	],

	// Axios module configuration: https://go.nuxtjs.dev/config-axios
	axios: {},

	// Build Configuration: https://go.nuxtjs.dev/config-build
	build: {
		transpile: ['lowlight', 'fault', 'lodash-es'],
		postcss: {
			plugins: {
				tailwindcss: {},
				autoprefixer: {},
			},
		},
	},

	googleFonts: {
		families: {
			'Material+Icons': true,
			'Roboto+Slab': [300, 400, 500, 600, 700],
		},
	},

	generate: {
		interval: 2000,
	},

	typescript: {
		shim: false,
	},

	bridge: {
		vite: true,
	},
});
